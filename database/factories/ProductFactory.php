<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence($nbWords = 4, $variableNbWords = true),
        'description'=> $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'image'=>'/uploads/book.png',
        'price' => $faker->numberBetween($min = 10, $max = 300),
        
    ];
});
