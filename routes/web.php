<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' ,'FrontEndController@index');



Route::resource('Product','ProductController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/singleproduct/{id}','FrontEndController@show')->name('single');

Route::post('/shop','ShoppingController@add')->name('addtoshoppingcart');
Route::get('/shop','ShoppingController@viewcart')->name('viewcart');

Route::get('/remove/{id}','ShoppingController@deleteitem');

Route::get('/cart/rem/{id}/{qty}','ShoppingController@removeoneitem');

Route::get('/cart/addy/{id}/{qty}','ShoppingController@addoneitem');

Route::get('/quickproduct/{id}','ShoppingController@quickadd');

Route::get('checkout','CheckoutController@checkout');

Route::post('/pay','CheckoutController@pay')->name('pay');
