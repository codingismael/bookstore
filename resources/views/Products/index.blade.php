@extends('layouts.app')


@section('content')

<div class="container">


	<div class="row">
		

       <table class="table table-striped">
  <thead>
    <tr>
      
      <th scope="col">name</th>
      <th scope="col">Price</th>
      <th scope="col">image</th>
      <th scope="col">edit</th>
      <th scope="col">delete</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($allProducts as $product)
    <tr>
      
      <td>{{$product->name}}</td>
      <td>{{$product->price}}</td>
      <td><img src="{{asset($product->image)}}" height=40 width=30></td>
      <td><a href="/Product/{{$product->id}}/edit" class="btn btn-sm btn-info">Edit</a></td>
      <td><form action="/Product/{{$product->id}}" method="post">@method('DELETE') @csrf<button class="btn btn-danger btn-sm" type="submit">Delete</button>  </form>  </td>
    </tr>
    @endforeach
  </tbody>
</table>

	{{$allProducts->links()}}


	</div>
	






</div>


@endsection