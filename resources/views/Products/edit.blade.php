@extends('layouts.app')



@section('content')


<div class="container">
	
   @if($errors->any())

     {{$errors}}

   @endif
	<div class="row">
		
		<form action="/Product/{{$theproduct->id}}" method="post" enctype="multipart/form-data">

			@method('put')

			@csrf

			<div class="form-group">
				
				<label for="image">Book name</label>
				<input type="text" name="name" class="form-control" value="{{$theproduct->name}}">
			</div>

			<div class="form-group">
    <label for="exampleFormControlTextarea1">Description of the Product</label>
    <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3" cols='100'>{{$theproduct->description}}</textarea>
  </div>
			<div class="form-group">
                              <label for="image">Price</label>
                              <input type="number" name="price" class="form-control" value="{{$theproduct->price}}">
</div>
			

						<div class="form-group">
                              <label for="image">Image</label>
                              <input type="file" name="image" class="form-control">
</div>
 <div class="form-group">	<button class="form-control btn btn-success">Update Product</button></div>






		</form>

	</div>
</div>

@endsection