<!DOCTYPE html>
<html lang="en">

@include('includes.headfrontend')




@include('includes.headerfrontend')


<div class="content-wrapper">

@include('includes.slogan')

    <!-- End Books products grid -->

@yield('page')
</div>

@include('includes.footerfrontend')


</body>

<!-- Mirrored from theme.crumina.net/html-seosight/16_shop.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Nov 2016 13:03:04 GMT -->
</html>