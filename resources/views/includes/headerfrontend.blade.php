<body class=" ">
<header class="header" id="site-header">

    <div class="container">

        <div class="header-content-wrapper">

            <ul class="nav-add">
                <li class="cart">

                    <a href="#" class="js-cart-animate">
                        <i class="seoicon-basket"></i>
                        <span class="cart-count">{{Cart::count()}}</span>
                    </a>

                    <a href="/shop"><div class="cart-popup-wrap">
                        <div class="popup-cart">
                            <h4 class="title-cart">{{Cart::count()}} products in the cart!</h4>
                            <p class="subtitle">${{Cart::total()}}</p>
                            <div class="btn btn-small btn--dark">
                                <span class="text">view cart</span>
                            </div>
                        </div>
                    </div></a>

                </li>
            </ul>
        </div>

    </div>

</header>