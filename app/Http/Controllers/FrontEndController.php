<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class FrontEndController extends Controller
{
    public function index(){

    	$allproducts = Product::paginate(6);

    	

    	return view('index')->with('allproducts',$allproducts);


    }

    public function show($id){

    	$theproduct = Product::find($id);

    	return view('singleproduct')->with('theproduct',$theproduct);
    }
}
