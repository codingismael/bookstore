<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use Mail;
use App\Mail\successfulpurchase;


class CheckoutController extends Controller
{
    public function checkout(){



    	return view('checkout');
    }

    public function pay(){


    	// Set your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
\Stripe\Stripe::setApiKey("sk_test_4IOKcwUL2UorU0BbnhE46nuN");

// Token is created using Checkout or Elements!
// Get the payment token ID submitted by the form:
$token = $_POST['stripeToken'];

// Charge the user's card:
$charge = \Stripe\Charge::create(array(
  "amount" =>Cart::total()*100,
  "currency" => "usd",
  "description" => "the book store",
  "source" => $token,
));

Mail::to(request()->stripeEmail)->send(new successfulpurchase());

Cart::destroy();

return redirect('/');

    }
}
