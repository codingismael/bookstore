<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $allProducts = Product::paginate(10);
        return view('products.index')->with('allProducts',$allProducts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

                'name'=>'required',
                'description'=>'required',
                'price'=>'required|numeric',
                'image'=>'required|image',


        ]);

        $newproduct = new Product;

        $newproduct->name = $request->name;
        $newproduct->description =  $request->description;
        $newproduct->price = $request->price;

        $productimage = $request->image;

        $newimagename = time().$productimage->getClientOriginalName();

        $productimage->move('uploads/products',$newimagename);

        $newproduct->image ='uploads/products/'.$newimagename;

        $newproduct->save();

        return redirect()->route('Product.index');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $theproduct = Product::find($id);

        
        return view('Products.edit')->with('theproduct',$theproduct);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;

        if($request->hasFile('image')){

            $productimage = $request->image;
            $newimagename = time().$productimage->getClientOriginalName();

            $productimage->move('uploads/products',$newimagename);
            $product->image = 'uploads/products'.$newimagename;
        }

        $product->save();

        return redirect()->Route('Product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if(file_exists($product->image)){

            unlink($product->image);
        }

        $product->delete();

        return redirect()->back();
    }
}
