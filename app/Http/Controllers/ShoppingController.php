<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Product;

class ShoppingController extends Controller
{
    

    public function add(){



    	$product = Product::find(request()->id);

    	

    	$thecart = Cart::add($product->id,$product->name,request()->quantity,$product->price);

    	$thecart->associate('App\Product');



    	return view('cart');


    }

    public function deleteitem($id){


    	Cart::remove($id);

    	return view('cart');
    }

    public function removeoneitem($id,$qty){


    	Cart::update($id,$qty-1);

    	return view('cart');
    }

        public function addoneitem($id,$qty){


    	Cart::update($id,$qty+1);

    	return view('cart');
    }

    public function viewcart(){


    	return view('cart');
    }

    public function quickadd($id){


    	$product = Product::find($id);

    	

    	$thecart = Cart::add($product->id,$product->name,1,$product->price);

    	$thecart->associate('App\Product');



    	return redirect()->back();
    }


}
